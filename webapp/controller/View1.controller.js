sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function (Controller) {
	"use strict";

	return Controller.extend("com.venkatesh.pieChart.controller.View1", {
		onInit: function () {
			var data = {
				"milk": [{
					"storeName": "a",
					"revenue": "10"
				}, {
					"storeName": "b",
					"revenue": "20"
				}, {
					"storeName": "c",
					"revenue": "70"
				}]
			};
			var dataModel = new sap.ui.model.json.JSONModel(data);
			this.getView().setModel(dataModel,"dataModel");
		}
	});
});